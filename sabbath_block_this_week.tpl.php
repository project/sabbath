<?php

/**
 * @file
 * Default template for sabbath block.
 *
 * Available variables:
 * - $sabbath_start: GMT timestamp for sabbath start.
 * - $sabbath_end: GMT timestamp for sabbath end.
 * - $start_offset: Seconds to offset start time from GMT.
 * - $end_offset: Seconds to offset end time from GMT.
 */
?>
<p><strong><?php print t('Begins') ?>:</strong><br />
<?php print format_date($sabbath_start, 'custom', 'l, g:i <\s\m\a\l\l>A</\s\m\a\l\l>', $start_offset) ?><br />
<?php print format_date($sabbath_start, 'custom', 'F j, Y', $start_offset) ?></p>
<p class="sabbath-this_week"><strong><?php print t('Ends') ?>:</strong><br />
<?php print format_date($sabbath_end, 'custom', 'l, g:i <\s\m\a\l\l>A</\s\m\a\l\l>', $end_offset) ?><br />
<?php print format_date($sabbath_end, 'custom', 'F j, Y', $end_offset) ?></p>
