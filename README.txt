
Disclaimer: This module has not been reviewed by any religious organization. No
endorsement of its use for religious purposes is implied.

The time that the sabbath begins and ends changes throughout the year because,
historically, a full day began at sunset. The Sabbath module provides the
sabbath start and end times for any week and any latitude, longitude, and time
zone.

It provides two API functions:

1. sabbath_time($latitude, $longitude, $timezone = NULL, $time = NULL, $future = 'time')

   This returns an object containing the sabbath start and end timestamps, the
   time zone offset for each, and the beginning of the next week. $future can be
   set to 'day' or 'time' and specifies whether to show the sabbath that ends
   after the given time or after the day in which the time occurs.

2. sabbath_now($latitude, $longitude, $timezone = NULL, $time = NULL)

   This returns a boolean value which tells whether the given time falls within
   the sabbath.

The Sabbath module also provides a block for displaying the local sabbath start
and end times for the current week.


Requirements
------------
PHP 5 or above


Credits
-------
Sunset photograph by Eiren Oh. Taken August 4, 2006 at Treasure Island, Florida.
